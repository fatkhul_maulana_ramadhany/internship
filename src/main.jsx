import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';

ReactDOM.createRoot(document.getElementById('root')).render(
    <React.StrictMode>
        <>
            <div className='px-[1.3rem] lg:py-[3rem] md:py-[2rem] lg:px-[3rem] md:px-[1rem] w-full flex lg:flex-row flex-col-reverse align-items-start justify-start'>
                <div className={'w-full lg:w-1/2'}>
                    <h1 className={'font-bold text-[43px] mb-5'}>
                        Dari Madrasah Untuk
                        <p></p>
                        Generasi Indonesia
                    </h1>
                    <h2 className={'font-medium text-[20px] '}>
                        <p className={'mb-8'}>
                            Wujudkan generasi berakhlaqul Karimah dengan haluan{' '}
                            <p></p>
                            Ahlusunnah Wal Jamaah
                        </p>
                    </h2>
                    <Button text='Hubungi Admin' />
                </div>
                <div className='w-full lg:w-1/2'>
                    <img
                        className='w-[500px]'
                        src='https://demo.teknolove.co/wp-content/uploads/2022/12/website-sekolah-website-yayasan-website-pondok-pesantren-Jasa-pembuatan-website-Harga-pembuatan-website-Jasa-pembuatan-website-murah-Biaya-pembuatan-website-murah-3.webp'
                    />
                </div>
            </div>
        </>
    </React.StrictMode>
);
function Button(props) {
    return (
        <button
            className={
                'w-[188px] bg-gradient-to-b from-[#0cc8c5] to-[#01787a] text-white font-medium text-[16px] px-[2rem] py-[9px] shadow-2xl rounded-[10px]'
            }>
            {props.text}
        </button>
    );
}
